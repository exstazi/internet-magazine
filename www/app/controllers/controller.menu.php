<?php
class ControllerMenu
{
	private $param;

	function __construct( $param ) {
		$this->param = $param;
		$this->view = new View();
	}

	function actionMenu() {		
		$this->param['script'] = '<script src="/assets/js/underscore.min.js"></script><script src="/assets/js/magazine.js"></script>';
		$this->view->generate( 'main_view.php', $this->param );
	}
}
?>