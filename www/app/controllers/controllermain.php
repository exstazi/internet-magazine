<?php
class ControllerMain extends Controller
{
	function action_index() {
		$param = Param::getData();
		$param['script'] = '<script src="/assets/js/underscore.min.js"></script><script src="/assets/js/menu.js"></script>';
		$this->view->generate( 'main_view.php', $param );
	}
}

?>