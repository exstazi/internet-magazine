<?php
class ControllerBasket
{
	function __construct($param) {
		$this->param = $param;
		$this->view = new View();
	}

	function actionBasket() {
		$this->param['script'] = '<script src="/assets/js/underscore.min.js"></script><script src="/assets/js/basket.js"></script>';
		$this->view->generate( 'basket.view.php', $this->param );
	}
}
?>