<?php
class Route
{
	static function start() {

		$routes = new Routes( Route::getQuery() );

		if ($routes->isEmpty()) {
			throw new RouteNotFound( $query );
		}

		$pathToController = 'app/controllers/controller.' . strtolower( $routes->getController() ) . '.php';

		if (!file_exists( $pathToController ) || $routes->getAccess() == 'visable') {
			throw new ControllerNotFound( $config['controller'] );			
		}

		require $pathToController;
		$controllerName = 'Controller' . ucfirst( $routes->getController() );
		$controller = new $controllerName($routes->getInfo() );
		$action = $routes->getAction();
		if (!method_exists( $controller, $action)) {
			throw new ActionControllerNotFound( $controllerName . ' - ' . $action );			
		}

		$controller->$action();
	}

	static function getQuery() {
		$query = 'menu';
		if ($_SERVER['REQUEST_URI'] != '/') {
			$url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
			$query = trim( $url_path, '/' );
		}
		return $query;
	}
}

?>