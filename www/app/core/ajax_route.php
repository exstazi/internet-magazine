<?php
class AjaxRoute {

	static function start() {
		if (empty($_POST['action'])) {
			$_POST = json_decode(file_get_contents('php://input'), true);
			if (empty($_POST['action'])) {
				throw new Exception( "action is empty" );				
			}
		}

		$action = strtolower( $_POST['action'] );
		$param = $_POST['param'];
		$routes = include( 'config/ajax_routes.php' );

		if (isset($routes[$action])) {
			$model = $routes[$action];
		} else {
			throw new Exception( "'".$action."' - action not found");	
		}

		$model = explode('/', $model);
		if (!file_exists( 'models/' . $model[0] .'.php' )) {
			throw new Exception("Model '".$model[0]." not fonds");			
		}
		require 'models/' . $model[0] . '.php';

		$model_name = ucfirst( $model[0] );
		$model_action = $model[1];

		if (method_exists($model_name, $model_action)) {
			echo json_encode( $model_name::$model_action($param) );
		} else {
			throw new ActionControllerNotFound( $model_name . ' - ' . $model_action );			
		}
	}
}