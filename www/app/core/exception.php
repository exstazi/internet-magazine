<?php

class ControllerNotFound extends Exception
{
	function __toString() {
		return __CLASS__ . ": [{$this->message}]\n";
	}
}

class RouteNotFound extends Exception
{
	function __toString() {
		return __CLASS__ . ": [{$this->message}]\n";
	}
}

class ActionControllerNotFound extends Exception
{
	function __toString() {
		return __CLASS__ . ": [{$this->message}]\n";
	}
}

?>