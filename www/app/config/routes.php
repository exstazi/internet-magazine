<?php
require_once 'app/components/local_storage.php';
class Routes
{
	private $route;

	function __construct( $url ) {
		if (strlen($url) < 3) {
			throw new Exception("Unccorect url");
		}
		$storage = new LocalStorage( 'app/config/routes.json' );
		$this->route = $storage->getById( $url );
	}

	function isEmpty() {
		return empty( $this->route );
	}

	function getController() {
		return $this->route['controller'];
	}

	function getAction() {
		return $this->route['action'];
	}

	function getAccess() {
		return $this->route['access'];
	}

	function getInfo() {
		return array_slice( $this->route, 3);
	}
}
?>