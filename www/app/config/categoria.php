<?php
return Array (
	Array
		(
			'name' => 'Роллы холодные',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Роллы запеченные',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Суши холодные',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Суши запеченные',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Разливное пиво',
			'units' => 'л'
		),
	Array
		(
			'name' => 'Наборы',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Кофе',
			'units' => 'мл'
		),
	Array
		(
			'name' => 'Десерты',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Напитки',
			'units' => 'мл'
		),
	Array
		(
			'name' => 'Детское меню',
			'units' => 'гр'
		),
	Array
		(
			'name' => 'Дополнительные добавки',
			'units' => 'гр'
		)
);
?>