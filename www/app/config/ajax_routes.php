<?php
return Array(
	'get-list-basket' => 'basket/getListProduct',
	'add-product-basket' => 'basket/addProduct',
	'change-count-product' => 'basket/changeCountProduct',
	'delete-product' => 'basket/deleteProduct',
	'clear-basket' => 'basket/clearBasket',

	'get-list-product' => 'products/getListProduct',
	'get-product' => 'products/getProduct',
	'add-product' => 'products/addProduct',
	'update-product' => 'products/update-product',


	//admin
	'get-list-info-page' => 'settings/getListInfoPage',
	'edit-info-page' => 'settings/setInfoPage'
);
?>