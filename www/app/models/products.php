<?php
require_once 'components/db.php';

class Products extends Model
{
	static function getListProduct($cat = Array()) {
		$cat = intval( $cat['id'] );
		$db = DB::getConnection();
		return $db->getAll( 'SELECT product.id, product.name, price, gram, img, ingredients, units FROM product, categoria WHERE product.cat=?i AND categoria.id=product.cat', $cat );
	}

	static function getProduct($param) {
		$db = DB::getConnection();
		return $db->getRow( "SELECT id, name, price, gram, img, ingredients FROM product WHERE id=?i", $param['id'] );
	}

	static function addProduct($product) {
		$db = DB::getConnection();
		$db->query( "INSERT INTO product SET cat=?i, name=?s, gram=?i, price=?i, ingredients=?s, img=?s",
			$product['cat'], $product['name'], $product['gram'], $product['price'], $product['ingredients'], $product['img'] );
	}

	static function updateProduct($product) {
		$db = DB::getConnection();
		//$db->query( "UPDATE product SET " );
	}
}