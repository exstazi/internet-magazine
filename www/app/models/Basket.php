<?php
session_start();

class Basket extends Model
{
	static function getListProduct() {
		if (!empty($_SESSION['basket'])) {
			$list = Array();
			foreach ($_SESSION['basket'] as $key => $value) {
				$list[] = $value;
			}
			return $list;
		}
		return Array();
	}

	static function addProduct($param) {
		$id = $param['id'];
		if (empty( $_SESSION['basket'][$id] )) {
			require_once 'components/db.php';
			$db = DB::getConnection();
			$product = $db->getRow('SELECT product.id, product.name, price, gram, img, ingredients, units FROM product, categoria WHERE product.id=?i AND product.cat = categoria.id', $id);
			$_SESSION['basket'][$id] = $product;
			$_SESSION['basket'][$id]['count'] = 1;
		} else {
			$_SESSION['basket'][$id]['count']++;
		}
	}

	static function changeCountProduct($param) {
		if (!empty( $_SESSION['basket'][$param['id']])) {
			$_SESSION['basket'][$param['id']]['count'] = $param['count'];
		}
	}

	static function deleteProduct($param) {
		unset( $_SESSION['basket'][$param['id']] );
	}

	static function clearBasket() {
		unset( $_SESSION['basket'] );
	}
}