<?php
require_once 'components/local_storage.php';

class Settings
{
	private static $pages;

	static function getListInfoPage($page = '') {
		if (empty(self::$pages)) {
			self::$pages = new LocalStorage( 'config/routes.json' );
		}
		if (empty($page)) {			
			return self::$pages->getAll();
		}
		return self::$pages->getById($page);
	}

	static function setInfoPage($params) {
		if (!empty( $params['pk'] )) {
			$page = Settings::getListInfoPage($params['pk']);
			$page[$params['name']] = $params['value'];
			self::$pages->update( $params['pk'], $page );
			self::$pages->flush();
			print_r( $page );
		}
		
	}
}
?>