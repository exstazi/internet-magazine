<?php

class Base
{
	static private $db;
	static private $settings;
	static private $page;

	static function init($db) {
		self::$db = $db;
		self::$settings = self::$db->getRow( "SELECT * FROM settings" );
	}

	static function setPage($page) {
		self::$page = self::$db->getRow( "SELECT * FROM pages WHERE url=?s", $page );
		if (empty(self::$page)) {
			throw new Exception( "Page not found" );
		}
	}

	static function getParams() {
		return self::$settings;
	}

	static function getDataPage() {
		return self::$page;
	}
}

?>