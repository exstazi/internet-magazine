<?php $data = array_merge( Param::getData(), $data ); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?=$data['title']?> - <?=$data['name']?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="<?=$data['keywords']?>" />
	<meta name="description" content="<?=$data['description']?>" />
	
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css" media="all" />
	<link rel="stylesheet" type="text/css" href="/assets/css/template.css" media="all" />	
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" media="all">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css" media="all" />

</head>
<body>
	
	<div class="top-bar" style="position: fixed; width: 100%; z-index: 100">		
		<div class="container"><div class="row">

			<div class="col-sm-12 top-right">
				<ul class="ul-header">
					<li><a  id="link-basket"  class="basket-button"  href="/basket"> Корзина </a></li>
					<li><i class="fa fa-phone"> </i> Контроль качества: <?=$data['control-phone'];?></li>	
					<li><i class="fa fa-phone"> </i> <?=$data['phone'];?></li> 
					<li> <i class="fa fa-clock-o"> </i> Режим работы: <?=$data['mode'];?></li>
					<li><?=$data['city'];?></li>								
				</ul>				
			</div>
		</div></div>
	</div>
	<br /><br />

	<header class="site-header">
		<div class="container"> <div class="row">
			<div class="col-sm-4"> 
				<div class="site-branding">
					<h1 class="site-title logo">
						<a href="/main"><img class="ft_logo" src="/assets/img/logo.jpg" width="250" /></a>
					</h1>
				</div>
			</div>

			<div class="col-sm-8"> 
				<nav id="site-navigation" class="main-navigation" role="navigation" >
					<div class="menu-restaurant-menu-container">
						<ul id="restaurant" class="menu">
							<li class="menu-item"><a href="/menu">Меню</a></li>
							<li class="menu-item"><a href="/shares">Акции</a></li>
							<li class="menu-item"><a href="/about">О нас</a></li>
							<li class="menu-item"><a href="/shipping">Доставка и оплата</a></li>
							<li class="menu-item"><a href="/contact">Контакты</a></li>
						</ul>
					</div>				
				</nav>				
			</div>	
		</div></div>
	</header>

	<div class="top-header">
		<div class="container"> 
			<h1><?=$data['title'];?></h1>
		</div>
	</div>
	
	<div class="container">
		<?php include 'app/views/' . $content_view; ?>
	</div>

	<footer class="site-footer">
		<div class="container"> <div class="row"> 
			<div class="col-md-12">
				<div class="site-info">
				Copyright &copy; 2016 <?=$data['name']?> <br>
				  <?=$data['city'];?>
				</div>
			</div>	
		</div></div>		
	</footer>

	<script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <?=$data['script'];?>
    

	<script type="text/javascript">
		if ($(document).height() <= $(window).height()) {
	  		$(".site-footer").addClass("navbar-fixed-bottom");
	  	}
	</script>
</body>
</html>