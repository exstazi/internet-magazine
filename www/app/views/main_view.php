<div class="row"> 
	<div class="col-md-3">
		<aside id="secondary" class="widget-area">

			<section class="widget widget_categories">
				<h2 class="widget-title">Категории</h2>	
				<ul>
					<?php
						$cat = include("app/config/categoria.php");						
						for ($i = 0; $i < sizeof( $cat ); ++$i) {
							echo "<li class='cat-item'>
									<a class='cat-a' data-id='".($i+1)."'>".$cat[$i]['name']."</a>
								  </li>";
						}
					?>
				</ul>
			</section>

			<section class="widget widget_categories">
				<h2 class="widget-title">Корзина</h2>
				<ul id="mini-basket">
				<!-- list mini basket-->
				</ul>					
					<a class='btn-basket' href='/basket'>Оформить заказ</a>
					<a class='btn-basket' id="clear-basket">Очистить корзину</a>	
			</section>
		</aside>
	</div>
	<div class="col-md-9">		
		<div class="blog-wrap">
			<div id="div-loading" class="well loading">
			    <img src="/assets/img/loading.gif" /><p>Загрузка меню...</p>
			</div>

			<div id="list-product-menu" class="row">
				<!-- list products-->
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="window-title"></h4>
			</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8"> 
							<img id="window-img" src="/img/1.jpg" />
						</div>
						<div class="col-md-4"> 
						<h2 id="window-price">  </h2>
						<span id="window-gram" class="label-gram-basket"></span>
						<a id="window-basket" class='btn-basket js-to-basket' data-dismiss="modal">В корзину</a>
						<h4> </h4>
						<p id="window-products"></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="item-menu-template" type="text/template">
    <% _.each(goods, function(good) { %>
    	<div class='col-md-4 item-product' >
			<div class='item-product-view' data-id='<%= good.id %>'> 
				<img class='js-open-modal js-main' data-id='<%= good.id %>' alt='<%= good.name %>' src='/img/<%= good.img %>' />								
				<span class='label-gram'> <%= good.gram %> <%= good.units %>.</span>
				<hr />
				<h3> <%= good.name %> </h3>
				<div class='item-price'> <%= good.price %> <i class='fa fa-ruble'></i> </div>
				<a class='btn-basket js-to-basket' data-id='<%= good.id %>'>В корзину </a>
			</div> 
			
		</div>
    <% }); %>
</script>

<script id="mini-basket-template" type="text/template">
    <% _.each(goods, function(good) { %>
    	<li> <%= good.name %>
		<div class='row'>
			<div class='col-md-5'>
					<img class='js-open-modal js-pos' data-id='<%= good.id %>' style='cursor: pointer' src='/img/<%= good.img %>' />
				</div>
				<div class='col-md-3'>
					x<%= good.count %>
				</div>
				<div class='col-md-4'>
					<%= good.price * good.count %> р.
				</div>
		</div></li>
    <% }); %>
</script>