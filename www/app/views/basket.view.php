<div class="wizard">
	<div class="steps-container">
		<ul class="steps">
			<li data-step="1" class="complete">
				Ваш заказ
				<span class="chevron"></span>
			</li>
			<li data-step="2" class="active">
				Оформление
				<span class="chevron"></span>
			</li>
			<li data-step="3">
				Завершение заказа
			</li>
		</ul>
	</div>
</div>

<div class="view-basket" style="display: block;">
	<table class="table-basket">
		<thead>
			<th>Блюдо</th>
			<th>Наименование</th>
			<th>Количество</th>
			<th>Стоимость</th>
			<th></th>		
		</thead>

		<tbody id="basket">
		    <tr><td colspan="6"><img src="/assets/img/loading.gif" /></td></tr>
		</tbody>

		<tr>
			<td><p>Обший вес: 1300 гр</p></td>
			<td></td>
			<td><h3>Общая сумма заказа</h3></td><td><h3><span id='total-summa'>0</span> р.</h3></td>
			<td></td>
		</tr>
	</table>


	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<button id="clear-basket" href="">Очистить корзину</button>
			</div>

			<div class="col-md-3 col-md-offset-3">
				<a class='btn-basket' href="/menu">Вернуться в меню</a>
			</div>
			<div class="col-md-2">
					<a class="btn-basket" href="/pickup">Самовывоз</a><br /><p class="text-center">Заберите когда удобно</p>
				</div>
				<div class="col-md-2">
					<a id="btn-delivery" class="btn-basket" href="/delivery">Доставка</a><br /><p class="text-center">Привезем домой в удобное время</p>
				</div>
		</div>
	</div>
</div>

<div class="view-delivery" style="display: none;">
	<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		<h2>Оформление доставки</h2>
			<table>
				<tr>
					<td><input type="text" class="form-control"  id="name" placeholder="Введите имя" /></td>
					<td>Способ оплаты: </td>
					<td><select id="payment" name="payment" class="form-control ">
							<option value="1">Наличными</option>
							<option value="0">По карте</option>
						</select> </td>
				</tr>
				<tr>
					<td><input type="text" class="form-control" id="phone" name="phone" placeholder="Введите телефон"/></td>
					<td>Сдача с купюры</td>
					<td>
						<div class="input-group">
						  <input id="bills" type="number" min="0" class="form-control " name="bills" />
						  <span class="input-group-addon"> <i class='fa fa-ruble' ></i></span>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="text" class="form-control" id="address" name="address" placeholder="Введите адрес"  /></td>
					<td>Количество персон </td>
					<td>
					<input id="count_person" class="form-control" type='number' value='1' name="count_person" /></td>
				</tr>
				<tr>
					<td colspan="3"><textarea id="comment" class="form-control" rows="5"  placeholder="Добавить комментарий к заказу..."></textarea></td>
				</tr>

				<tr style="background-color: #eee">
					<td><h3>Общая сумма заказа</h3><h3>Доставка</h3></td>
					<td></td>
					<td><h3><?=$totalPrice;?> р.</h3> <h3>0 р.</h3></td>
				</tr>

				<tr style="background-color: #ccc">
					<td><h3>Итого</h3></td>
					<td></td>
					<td><h3><?=$totalPrice;?> р.</h3></td>
				</tr>

				<tr>
					<td></td>
					<td></td>
					<td><button id="checkout" class='btn-basket'>Оформить заказ</button></td>
				</tr>
				
			</table>

		</div>
		</div>
		</div>			

</div>

<div class="view-pickup">

</div>

<div class="view-finish">

</div>




<script id="basket-template" type="text/template">
    <% _.each(goods, function(good) { %>
    	<tr class="js-cart-item" data-id="<%= good.id %>">
    		<td width=180><img src='img/<%= good.img %>' /></td>
			<td><h3> <%= good.name %></h3>Вес: <%= good.gram %> <%= good.units %>.</td>							
			<td valign='middle' align='center'>
				<span 
                    class="js-change-count" 
                    title="Уменьшить на 1" 
                    data-id="<%= good.id %>" 
                    data-delta="-1">                
                    <i class="fa fa-minus btn"></i>
                </span>
                <span class="js-count"><%= good.count %></span>
                <span 
                    class="js-change-count" 
                    title="Увеличить на 1" 
                    data-id="<%= good.id %>" 
                    data-delta="1">
                    <i class="fa fa-plus btn"></i>
                </span>
			</td>
			<td valign='middle'>
				<h4><span class="js-summa"><%= good.count * good.price %></span> р.</h4>
			</td>
			<td>
                <span class="cart-item__btn-remove js-remove-from-cart" title="Удалить из корзины" data-id="<%= good.id %>">
                    <i class="fa fa-times btn"></i>                             
                </span>
            </td>
        </tr>
   	 <% }); %>
</script>