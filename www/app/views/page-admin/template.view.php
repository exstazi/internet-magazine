<!DOCTYPE html>
<html>
<head>
	<title>Админ панель</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" media="all" />
	<link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-table.css">
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-editable.css">

</head>
<body>

<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Admin</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/admin/main">Страницы</a></li>
        <li><a href="#">Custom Page</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Order <span class="label label-warning">5</span></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Backup db</a></li>
            <li><a href="#">Backup config</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
	<div class="row">		
		<div class="col-md-12">			
		    <table id="table" class="table table-striped"
				data-search="true"
				data-sortable="true"
				data-show-refresh="true"
				data-show-columns="true"
				data-sort-name="id"
				data-page-list="[5, 10, 20]"
				data-page-size="5"
				data-pagination="true" 
				data-show-pagination-switch="true"
				data-checkbox="true"
				data-click-to-select="true"
			    data-filter-control="true" >
			</table>
		</div>
	</div>	
</div>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap/bootstrap-table.js"></script>
<script src="/assets/js/bootstrap/bootstrap-table-editable.js"></script>
<script src="/assets/js/bootstrap/bootstrap-editable.js"></script>

<script type="text/javascript">
    $(function () {
        $('#table').bootstrapTable({
            idField: 'routes',
            method: 'post',
            url: '/app/ajax.php',
            showFooter: true,
            footerStyle:  function (value, row, index) {
                console.log( value );
              return {
                css: { "font-weight": "bold"}
              };
            },
            queryParams: {
                action: 'get-list-info-page'
            },
            responseHandler: function( data ) {
                var res = [];
                for(var obj in data) {
                    res.splice( 0, 0,  $.extend( {routes: obj}, data[obj]) );
                }
                return res;
            },
            columns: [{
                field: 'state',
                checkbox: true
            }, {
                field: 'routes',
                title: 'url',
                sortable: true
            }, {
                field: 'action',
                title: 'action',
                sortable: true,
                footerFormatter: function() {
                    return '<input type=text>';
                },
                editable: {
                    type: 'text',
                    url: '/app/ajax.php',
                    params: function(params) {
                        return {
                            action: 'edit-info-page',
                            param: params
                        }
                    }
                }
            }, {
                field: 'access',
                title: 'access',
                sortable: true,
                editable: {
                    type: 'select',
                    source: [
                    	{value: 'visable', text: 'Закрыта'}, {value: 'empty-basket', text: 'Не пустая корзина'}                       
                    ],
                    url: '/app/ajax.php',
                    params: function(params) {
                        return data = {
                            action: 'edit-info-page',
                            param: params
                        }
                    }
                }
            }, {
                field: 'title',
                title: 'title',
                sortable: true,
                editable: {
                    type: 'text',
                    url: '/app/ajax.php',
                    params: function(params) {
                        return data = {
                            action: 'edit-info-page',
                            param: params
                        }
                    }
                }
            }, {
                field: 'keyword',
                title: 'keyword',
                sortable: true,
                editable: {
                    type: 'textarea',
                    url: '/app/ajax.php',
                    params: function(params) {
                        return data = {
                            action: 'edit-info-page',
                            param: params
                        }
                    }
                }
            }, {
                field: 'description',
                title: 'description',
                sortable: false,
                editable: {
                    type: 'textarea',
                    url: '/app/ajax.php',
                    params: function(params) {
                        return data = {
                            action: 'edit-info-page',
                            param: params
                        }
                    }
                }
            }
            ]
        });
    });	
</script>

</body>
</html>