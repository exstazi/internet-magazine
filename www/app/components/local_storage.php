<?php
class LocalStorage
{
	private $path;
	private $jsonObject;

	function __construct($p) {
		$this->path = $p;
		$this->jsonObject = json_decode( file_get_contents( $this->path ), true );
	}	

	public function getById( $id ) {
		return $this->jsonObject[$id];
	}

	public function getAll() {
		return $this->jsonObject;
	}

	public function update( $id, $value ) {
		$this->jsonObject[$id] = $value;
	}

	public function updateAll( $values = Array() ) {
		if (sizeof($values) > 0) {
			foreach ($values as $key => $value) {
				$this->jsonObject[$key] = $value;
			}
			$this->flush();
		}
	}

	public function flush() {
		file_put_contents( $this->path, json_encode( $this->jsonObject ) );
	}
}
?>
