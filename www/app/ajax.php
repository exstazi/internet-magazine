<?php
	require 'core/exception.php';
	require 'core/model.php';
	require 'core/ajax_route.php';

	try {
		AjaxRoute::start();
	} catch (Exception $e) {
		echo '[{"status": "error", "message": "'.$e->getMessage().'"}]';
	}
?>