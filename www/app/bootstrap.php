<?php
	require 'core/exception.php';
	require 'models/global.php';
	require 'core/controller.php';
	require 'core/model.php';
	require 'core/view.php';
	require 'config/routes.php';
	require 'core/route.php';

	require 'components/db.php';
	require 'models/base.php';

	try {
		Base::init( DB::getConnection() );
		Route::start();		
	} catch (Exception $e) {
		require 'views/page-error/404.php';
	}
?>